(function(){
    angular
        .module("GMS")
        .controller("SearchCtrl", SearchCtrl);

 SearchCtrl.$inject = [ '$window', '$state', '$filter','searchService'];

    function SearchCtrl($window, $state, $filter, searchService){
        var vm = this;

        vm.searchString = '';
        vm.result = null;
        vm.showManager = false;
        vm.grocery_list = [];
        
        



       // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.
        vm.goEdit = goEdit;
        vm.search = search;
        
        

        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        // init is a private function (i.e., not exposed)
        //init();

        // Function declaration and definition -------------------------------------------------------------------------
        function goEdit(product){
            $state.go("editWithParam",{brand : brand});
        }


        
        function search() {
            // We call searchService.retrieveBrand to handle retrieval of department information. The data retrieved
            // from this function is used to populate search.html. Since we are initializing the view, we want to
            // display all available departments, thus we ask service to retrieve '' (i.e., match all)
            searchService
                .retrieveBrand(vm.searchString)
                .then(function (result) {
                    // The result returned by the DB contains a data object, which in turn contains the records read
                    // from the database
                    console.log("results: " + JSON.stringify(result.data));
                    vm.grocery_list = result.data;
                    console.log(vm.grocery_list);
                })
                .catch(function (err) {
                    console.log("error " + err);
                });
        }

        function edit() {
            $state.go("Edit", {id: id});
        }


    }
})();