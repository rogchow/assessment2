(function(){
    angular
        .module("GMS")
        .controller("editCtrl", editCtrl);

    editCtrl.$inject = [ '$window', '$state', '$filter','$stateParams','searchService'];

    function editCtrl($window, $state, $filter, $stateParams,searchService){
        var vm = this;

        vm.searchString = '';
        vm.result = null;
        vm.showManager = false;
        vm.id = $stateParams.id;
        vm.update = update;
        vm.cancel = cancel;
        vm.grocery_list = {};
        console.log("vm.id: " + vm.id);



       // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.
        //vm.goEdit = goEdit;
        //vm.search = search;
        
        

        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        // init is a private function (i.e., not exposed)
        //init();

        function update(){
        console.log ("executing update function");
        var record = {
            name: vm.grocery_list.name,
            brand: vm.grocery_list.brand,
            UPC12: vm.grocery_list.UPC12
        }
        searchService
        .updateRecord(vm.id, record)
        .then(function(res) {
                    //res.message("record updated!")
                    // Success. Go back to prev page.
                    $state.go("search");
                })
                .catch(function(err) {
                    console.log(err);
                })
        }

         function cancel() {
            $state.go("search");
        }
    
        searchService
            .editRecord(vm.id)
            .then(function (result) {
                // The result returned by the DB contains a data object, which in turn contains the records read
                // from the database
                console.log("results: " + JSON.stringify(result.data));
                vm.grocery_list = result.data;
            })
            .catch(function (err) {
                console.log("error " + err);
            });
        }
})();