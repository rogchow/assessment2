(function(){
    angular
        .module("GMS")
        .service("searchService", searchService);


    searchService.$inject = ["$http"];

    function searchService($http){
        var service = this;
        service.retrieveBrand = retrieveBrand;
        service.editRecord = editRecord;
        service.updateRecord = updateRecord;
 
        function editRecord(id){
            console.log("in editRecord id=" + id);
            return $http({
                method: 'get',
                url: "api/edit/",
                params: {
                    'id': id
                }
            });

        }

    
        function retrieveBrand(searchString){
            return $http({
                method: 'get',
                 url: "api/search" ,
                 params: {
                    'searchString': searchString
                 }
            });
            
        }

         function updateRecord(id, record) {
            console.log("the current record is " + id);
            console.log(record);
            return $http({
                method: 'put',
                url: "api/update/" + id,
                data: record
            });
        }
    };
})()