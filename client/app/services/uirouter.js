(function () {
    angular
        .module("GMS")
        .config(Config);
            Config.$inject = ["$stateProvider","$urlRouterProvider"];

    function Config($stateProvider,$urlRouterProvider){
        $stateProvider
            .state('search', {
                url: '/search',
                templateUrl: './app/search/search.html',
                controller: 'SearchCtrl',
                controllerAs: 'ctrl'
            })
            
           .state('edit', {
                url: '/edit/:id',
                templateUrl: './app/edit/edit.html',
                controller: 'editCtrl',
                controllerAs: 'ctrl'
            })

            

    $urlRouterProvider.otherwise("/search");
}

})()