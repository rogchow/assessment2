// Model for testing table
// References:
// http://docs.sequelizejs.com/en/latest/docs/getting-started/#your-first-model
// http://docs.sequelizejs.com/en/latest/docs/models-definition/
//Create a model for testing table
module.exports = function(sequelize, Sequelize) {
    var grocery =  sequelize.define('grocery_list', {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },

        UPC12: {
            type: Sequelize.BIGINT,
            allowNull: false,
        },

        brand: {
            type: Sequelize.STRING,
            allowNull: false
       },

       name: {type: Sequelize.STRING,
            allowNull: false
              }
        
    },

    {
        timestamps : false,
        freezeTableName : true
    }
    
    );

    return grocery;

    }