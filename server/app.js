var express = require("express");
var bodyParser = require("body-parser");
var path = require("path");

// Loads sequelize ORM
var Sequelize = require("sequelize");

const NODE_PORT = process.env.NODE_PORT || 3000;
const CLIENT_FOLDER = path.join(__dirname, "../client");
//const IMG_FOLDER = path.join(__dirname, "../images");
//const MSG_FOLDER = path.join(__dirname, "../client/assets/messages");

const API_SEARCH_ENDPOINT = "/api/search";
const API_EDIT_ENDPOINT = "/api/edit";

// Defines MySQL configuration
const MYSQL_USERNAME = 'root';
const MYSQL_PASSWORD = 'MyNewPass';

var app = express();

var sequelize = new Sequelize(
        'GROCERY_LIST',
        MYSQL_USERNAME,
        MYSQL_PASSWORD,
        {
            host: 'localhost',         // default port    : 3306
            logging: console.log,
            dialect: 'mysql',
            pool: {
                max: 5,
                min: 0,
                idle: 10000
            }
        }
    );

    // Loads model for grocery table
var grocery = require('./models/grocery')(sequelize, Sequelize);

app.use(express.static(CLIENT_FOLDER));
//app.use("/uploads", express.static(IMG_FOLDER));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));



app.get("/api/search", function (req, res) {
    console.log("excution one test");
    console.log(req.query.searchString);

    grocery
    // findAll asks sequelize to search
        .findAll({
            where: {
                // This where condition filters the findAll result so that it only includes brand and
                // product that have the searchstring as a substring (e.g., if user entered 's' as search
                // string, the following
                $or: [
                    {brand: {$like: "%" + req.query.searchString + "%"}},
                    {name: {$like: "%" + req.query.searchString + "%"}},
                ],
                // inlcude: [{
                //     table: grocery,
                //     
                // }],
                  
            },
            limit: 20
        })
        .then(function (grocery) {
            res
                .status(200)
                .json(grocery);
        })
        .catch(function (err) {
            res
                .status(500)
                .json(err);
        });
        console.log("executed findall")
    
});

app.get("/api/edit", function(req,res){
    console.log("executing edit retriever");
    var where = {};
    console.log(req.query.id);
    if (req.query.id) {
        where.id = req.query.id
    }
    grocery
    // findOne asks sequelize to search one record
        .findOne({
            where: where
        })
         .then(function (grocery) {
            res
                .status(200)
                .json(grocery);
        })
        .catch(function (err) {
            res
                .status(500)
                .json(err);
        });
        
})

app.put("/api/update/:id", function(req,res){
    //console.log(req.params.id);
    //console.log(req.body);
     console.log("executing update");
     //var where = {};
     var where = {id: req.params.id};
     console.log("Update Data: " + JSON.stringify(req.body));
     //if (req.body.id){(where.id = req.body.id)}
     grocery
     .update({
            name: req.body.name,
            brand: req.body.brand,
            UPC12: req.body.UPC12

        }, {
            where: where
        })
          .then(function (grocery) {
             res
                 .status(200)
                 .json(grocery);
         })
         .catch(function (err) {
             res
                 .status(500)
                 .json(err);
         });

})



app.listen(NODE_PORT, function() {
    console.log("App started at port: %s", NODE_PORT);
});